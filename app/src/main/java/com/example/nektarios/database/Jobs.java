package com.example.nektarios.database;

/**
 * Created by nektarios on 1/27/16.
 */
public class Jobs {

    private String parameters;
    private String IP;
    private String periodic;
    private String time;
    private String position;

    public Jobs(){
    }

    public Jobs(String parameters, String IP, String periodic, String time, String position){
        this.setParameters(parameters);
        this.setIP(IP);
        this.setPeriodic(periodic);
        this.setTime(time);
        this.setPosition(position);
    }


    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getPeriodic() {
        return periodic;
    }

    public void setPeriodic(String periodic) {
        this.periodic = periodic;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
